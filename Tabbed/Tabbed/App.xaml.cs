﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Xamarin.Essentials;

namespace Tabbed
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            Preferences.Remove("numCorrect");
            Preferences.Remove("numWrong");
            MainPage = new TabbedPage1();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
