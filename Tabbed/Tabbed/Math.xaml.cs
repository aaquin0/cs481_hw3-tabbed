﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Xamarin.Essentials;

namespace Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Math : ContentPage
    {
        int wrong;
        int correct;

        int var1;
        int var2;
        int sign;
        int answer;

        Random rand = new Random();

        public Math()
        {
            InitializeComponent();
        }

        // when the user finishes their input, checks to see if it is correct here
        // calls updateScore at the end to update the score
        // 1 is addition, 2 is subtraction, 3 is multiplication, 4 is division
        // answers are all rounded down to the nearest int
        private void MathCompleted(object sender, EventArgs e)
        {
            int input = Int32.Parse(((Entry)sender).Text);

            switch(sign)
            {
                case 1:
                    answer = var1 + var2;
                    updateScore(answer, input);
                    break;
                case 2:
                    answer = var1 - var2;
                    updateScore(answer, input);
                    break;
                case 3:
                    answer = var1 * var2;
                    updateScore(answer, input);
                    break;
                case 4:
                    answer = var1 / var2;
                    updateScore(answer, input);
                    break;
                default:
                    Console.WriteLine("No operator found.");
                    break;
            }

            generateNew();
            mathEntry.Text = "";
        }

        private void updateScore(int x, int y)
        {
            if (x == y)
            {
                correct++;
                numCorrect.Text = "# Correct = " + correct;
            }
            else
            {
                wrong++;
                numWrong.Text = "# Wrong = " + wrong;
            }
        }

        private void generateNew()
        {

            var1 = rand.Next(1, 1001);
            var2 = rand.Next(1, 1001);
            sign = rand.Next(1, 5);

            leftVar.Text = var1.ToString();
            rightVar.Text = var2.ToString();

            switch (sign)
            {
                case 1:
                    signText.Text = "+";
                    break;
                case 2:
                    signText.Text = "-";
                    break;
                case 3:
                    signText.Text = "*";
                    break;
                case 4:
                    signText.Text = "/";
                    break;
                default:
                    Console.WriteLine("Sign didn't change.");
                    break;
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            generateNew();
            correct = Preferences.Get("numCorrect", 0);
            numCorrect.Text = "# Correct = " + correct;
            wrong = Preferences.Get("numWrong", 0);
            numWrong.Text = "# Wrong = " + wrong;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            Preferences.Set("numCorrect", correct);
            Preferences.Set("numWrong", wrong);
        }

    }
}