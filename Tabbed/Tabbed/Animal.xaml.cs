﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Xamarin.Essentials;

namespace Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Animal : ContentPage
    {
        string[] dogList = new string[] { "Beagle", "Boxer", "Chihuahua", "Dalmation", "Golden Retriever", "Pug", "Shiba Inu", "Siberian Husky", "Yorkshire Terrier" };
        Random rand = new Random();
        int randDog, randCorrectButton, correct, wrong;


        public Animal()
        {
            InitializeComponent();
        }

        // randomizes the dog that is shown, including the buttons underneath it
        // the same dog can't be shown twice in a row
        private void DogRandomizer()
        {
            randDog = rand.Next(0, 9);
            randCorrectButton = rand.Next(0, 4);

            int randomNumber;
            List<int> dogChoices = new List<int>();
            dogChoices.Add(randDog);

            // adds the dog picture
            dogPicture.Source = GetDogFileName(randDog);

            for(int i = 0; i < 3; i++)
            {
                do
                {
                    randomNumber = rand.Next(0, 9);
                } while (dogChoices.Contains(randomNumber));
                dogChoices.Add(randomNumber);
            }

            // adds choices to buttons
            if(randCorrectButton == 0)
            {
                dogButton0.Text = dogList[randDog];
                dogChoices.Remove(randDog);

                dogButton1.Text = dogList[dogChoices[0]];
                dogChoices.RemoveAt(0);
                dogButton2.Text = dogList[dogChoices[0]];
                dogChoices.RemoveAt(0);
                dogButton3.Text = dogList[dogChoices[0]];
                dogChoices.RemoveAt(0);
            }
            else if(randCorrectButton == 1)
            {
                dogButton1.Text = dogList[randDog];
                dogChoices.Remove(randDog);

                dogButton0.Text = dogList[dogChoices[0]];
                dogChoices.RemoveAt(0);
                dogButton2.Text = dogList[dogChoices[0]];
                dogChoices.RemoveAt(0);
                dogButton3.Text = dogList[dogChoices[0]];
                dogChoices.RemoveAt(0);
            }
            else if(randCorrectButton == 2)
            {
                dogButton2.Text = dogList[randDog];
                dogChoices.Remove(randDog);

                dogButton0.Text = dogList[dogChoices[0]];
                dogChoices.RemoveAt(0);
                dogButton1.Text = dogList[dogChoices[0]];
                dogChoices.RemoveAt(0);
                dogButton3.Text = dogList[dogChoices[0]];
                dogChoices.RemoveAt(0);
            }
            else
            {
                dogButton3.Text = dogList[randDog];
                dogChoices.Remove(randDog);

                dogButton0.Text = dogList[dogChoices[0]];
                dogChoices.RemoveAt(0);
                dogButton1.Text = dogList[dogChoices[0]];
                dogChoices.RemoveAt(0);
                dogButton2.Text = dogList[dogChoices[0]];
                dogChoices.RemoveAt(0);
            }
            
        }

        private string GetDogFileName(int x)
        {
            switch (x)
            {
                case 0:
                    return "beagle";
                case 1:
                    return "boxer";
                case 2:
                    return "chihuahua";
                case 3:
                    return "dalmatian";
                case 4:
                    return "golden_retriever";
                case 5:
                    return "pug";
                case 6:
                    return "shiba_inu";
                case 7:
                    return "siberian_husky";
                case 8:
                    return "yorkshire_terrier";
                default:
                    return "oops";
            }
        }

        private void DogClicked0(object sender, EventArgs e)
        {
            if(randCorrectButton == 0)
            {
                correct++;
                numCorrect.Text = "# Correct = " + correct;
            }
            else
            {
                wrong++;
                numWrong.Text = "# Wrong = " + wrong;
            }
            DogRandomizer();
        }
        private void DogClicked1(object sender, EventArgs e)
        {
            if (randCorrectButton == 1)
            {
                correct++;
                numCorrect.Text = "# Correct = " + correct;
            }
            else
            {
                wrong++;
                numWrong.Text = "# Wrong = " + wrong;
            }
            DogRandomizer();
        }
        private void DogClicked2(object sender, EventArgs e)
        {
            if (randCorrectButton == 2)
            {
                correct++;
                numCorrect.Text = "# Correct = " + correct;
            }
            else
            {
                wrong++;
                numWrong.Text = "# Wrong = " + wrong;
            }
            DogRandomizer();
        }
        private void DogClicked3(object sender, EventArgs e)
        {
            if (randCorrectButton == 3)
            {
                correct++;
                numCorrect.Text = "# Correct = " + correct;
            }
            else
            {
                wrong++;
                numWrong.Text = "# Wrong = " + wrong;
            }
            DogRandomizer();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            DogRandomizer();
            correct = Preferences.Get("numCorrect", 0);
            numCorrect.Text = "# Correct = " + correct;
            wrong = Preferences.Get("numWrong", 0);
            numWrong.Text = "# Wrong = " + wrong;


        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            Preferences.Set("numCorrect", correct);
            Preferences.Set("numWrong", wrong);
        }
    }
}